
Things to consider:

**Cheapest (1-2x)**
(motherboard + memory):

| Part        | Links                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Price    | Total        |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------ |
| Motherboard | [link](https://allegro.cz/nabidka/msi-b85m-g43-s1150-ddr3-crossfire-xmp-13549320668?utm_medium=cpc&utm_source=heureka.cz&utm_feed=52a03cc8-d311-4280-8b74-f27eebbedb53)                                                                                                                                                                                                                                                                                                                | 1599 CZK |              |
| CPU         | --                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |          |              |
| Memory      | [link](https://www.ebay.com/itm/325918347245?itmmeta=01HQKD5DKVHVK7HWHZVTQ52EC5&hash=item4be23f4fed:g:o5sAAOSwf9RlRgn0&itmprp=enc%3AAQAIAAAA4C6suLgL0UFTOkjv69Edq1l39ginBIvVMkHOvfyM0jPDfxUmSDTTkAjskL0CGngJsQReygZ9qe3jQyjkO5TMPQ6V%2BR7C7rv%2FKMRQVl9iqH6VCKUZhigzozd2xZAo5cE5JN98M1%2FdMVPdRvG6qymOK0yOfkfb0dg%2FZaz22b2XptAd7nW6yNP8zIZNkUIeMhFYHXzMoq7QNc7KWmOfB0QzKs6XTG33lXvFle9jYBseNfaqV5j4qcrSYI83gDMlWU7wYEg9Eiz2ghppcjdA0UnCohx3jxwqPVNsp9iRGYUSmucM%7Ctkp%3ABFBMkNqV7bxj) | 1776 CZK |              |
|             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |          | **3375 CZK** |

**Cheaper (1-3x)**
(motherboard + memory + faster disk):

| Part         | Links                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Price    | Total        |
| ------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------ |
| Motherboard  | [link](https://allegro.cz/nabidka/msi-b85m-g43-s1150-ddr3-crossfire-xmp-13549320668?utm_medium=cpc&utm_source=heureka.cz&utm_feed=52a03cc8-d311-4280-8b74-f27eebbedb53)                                                                                                                                                                                                                                                                                                                | 1599 CZK |              |
| CPU          | --                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |          |              |
| Memory       | [link](https://www.ebay.com/itm/325918347245?itmmeta=01HQKD5DKVHVK7HWHZVTQ52EC5&hash=item4be23f4fed:g:o5sAAOSwf9RlRgn0&itmprp=enc%3AAQAIAAAA4C6suLgL0UFTOkjv69Edq1l39ginBIvVMkHOvfyM0jPDfxUmSDTTkAjskL0CGngJsQReygZ9qe3jQyjkO5TMPQ6V%2BR7C7rv%2FKMRQVl9iqH6VCKUZhigzozd2xZAo5cE5JN98M1%2FdMVPdRvG6qymOK0yOfkfb0dg%2FZaz22b2XptAd7nW6yNP8zIZNkUIeMhFYHXzMoq7QNc7KWmOfB0QzKs6XTG33lXvFle9jYBseNfaqV5j4qcrSYI83gDMlWU7wYEg9Eiz2ghppcjdA0UnCohx3jxwqPVNsp9iRGYUSmucM%7Ctkp%3ABFBMkNqV7bxj) | 1776 CZK |              |
| New disk     | [link](https://www.alza.cz/wd-black-sn770-nvme-500gb-d6994484.htm)                                                                                                                                                                                                                                                                                                                                                                                                                     | 1719 CZK |              |
| Disk adapter | [link](https://www.alza.cz/orico-m2ts-sv-d5674229.htm)                                                                                                                                                                                                                                                                                                                                                                                                                                 | 329 CZK  |              |
|              |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |          | **5423 CZK** |

**Medium (1-4x)**
(motherboard + cpu + memory):

| Part        | Links                                                                                   | Price    | Total        |
| ----------- | --------------------------------------------------------------------------------------- | -------- | ------------ |
| Motherboard | [link](https://www.czc.cz/asus-prime-b550-plus-amd-b550/289974/produkt)                 | 2689 CZK |              |
| CPU         | [link](https://www.czc.cz/amd-ryzen-5-4500/341760/produkt)                              | 1859 CZK |              |
| Memory      | [link](https://www.czc.cz/kingston-fury-beast-black-32gb-ddr4-3200-cl16/323122/produkt) | 1819 CZK |              |
|             |                                                                                         |          | **6367 CZK** |

**Better (2.5-4x)**
(motherboard + cpu + memory + disk):

| Part        | Links                                                                                   | Price    | Total        |
| ----------- | --------------------------------------------------------------------------------------- | -------- | ------------ |
| Motherboard | [link](https://www.czc.cz/asus-prime-b550-plus-amd-b550/289974/produkt)                 | 2689 CZK |              |
| CPU         | [link](https://www.czc.cz/amd-ryzen-5-4500/341760/produkt)                              | 1859 CZK |              |
| Memory      | [link](https://www.czc.cz/kingston-fury-beast-black-32gb-ddr4-3200-cl16/323122/produkt) | 1819 CZK |              |
| New disk    | [link](https://www.alza.cz/wd-black-sn770-nvme-500gb-d6994484.htm)                      | 1719 CZK |              |
|             |                                                                                         |          | **8086 CZK** |

**Best (2-6x)**
(even faster cpu):

| Part        | Links                                                                                   | Price    | Total        |
| ----------- | --------------------------------------------------------------------------------------- | -------- | ------------ |
| Motherboard | [link](https://www.czc.cz/asus-prime-b550-plus-amd-b550/289974/produkt)                 | 2689 CZK |              |
| CPU         | [link](https://www.czc.cz/amd-ryzen-5-5600/341758/produkt)                              | 3290 CZK |              |
| Memory      | [link](https://www.czc.cz/kingston-fury-beast-black-32gb-ddr4-3200-cl16/323122/produkt) | 1819 CZK |              |
| New disk    | [link](https://www.alza.cz/wd-black-sn770-nvme-500gb-d6994484.htm)                      | 1719 CZK |              |
|             |                                                                                         |          | **9517 CZK** |
